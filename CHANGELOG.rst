
Changelog
=========
..
   All enhancements and patches to Tootbot will be documented
   in this file. It adheres to the structure of http://keepachangelog.com/ ,
   but in reStructuredText instead of Markdown (for ease of incorporation into
   Sphinx documentation and the PyPI description).

   This project adheres to Semantic Versioning (http://semver.org/).

Unreleased
----------

See the fragment files in the `changelog.d directory`_.

.. _changelog.d directory: https://codeberg.org/PyYtTools/Playlist2Podcasts/src/branch/main/changelog.d


.. scriv-insert-here

.. _changelog-8.2.0:

8.2.0 — 2023-12-15
==================

Changed
-------

- Uploading more than 4 attachments / images with a post if the instance server supports it.

- Updated CI files to use latest nox / python versions

.. _changelog-8.1.3:

8.1.3 — 2023-12-15
==================

Changed
-------

- Updated dependencies versions

.. _changelog-8.1.2:

8.1.2 — 2023-12-08
==================

Changed
-------

- Changed formatter to `ruff format`
- Updated dependencies versions

.. _changelog-8.1.1:

8.1.1 — 2023-10-22
==================

Added
-----

- Running CI check for vulnerabilities on a weekly basis

Changed
-------

- Updated dependencies versions

Removed
-------

- "dev" and "docs" dependencies. Those are now covered within nox


.. _changelog-8.1.0:

8.1.0 — 2023-10-15
==================

Changed
-------

- Updated dependencies versions

- Randomize the order in which posts are picked for posting (`issue #78`_)

.. _issue #78: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/78

Fixed
-----

- Despite new content, tootbot pulling from one subreddit over all others (`issue #77`_)

.. _issue #77: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/77

.. _changelog-8.0.1:

8.0.1 — 2023-08-26
==================

Changed
-------

- Refactored code determining text of post to reduce complexity a little.
- Updated CI config
- Dependencies versions updated

.. _changelog-8.0.0:

8.0.0 — 2023-07-23
==================

Breaking
--------

- Documentation for tootbot has moved to https://marvinsmastodontools.codeberg.page/tootbot/ and the Wiki has been disabled.

Added
-----

- Added an optional config section to list redditors / reddit user names to follow. This implements the feature request
  in issue #73.

  Posts made by these reddit users will be cross posted regardless of which sub reddit they have been posted in.

  This section is optional and with the introduction of this new option, the Subreddits section is now optional
  as well. However you must specify at least one Subreddit or Redditor to follow.

  As with the Subreddits section, you can optionally specify hashtags to add to any cross posts from each redditor.

  In the example below, posts by the redditor "Sunkisty" would have the "#variety" hash tag add to any cross posts.
  To configure this add a section as below to your config:

  .. codeblock:: ini
    [Redditors]
    Sunkisty: variety

Changed
-------

- Changed from using the submission short link to using permalink instead. This should address issue #74

- With the introduction of the `[Redditors]` section the Subreddits config section is now optional.
  However you must specify at least one Subreddit or Redditor to follow.

.. _changelog-7.3.0:

7.3.0 — 2023-07-13
==================

Breaking
--------

- Configuration keys in config.ini file are now case sensitive.

Added
-----

- Added an optional config section to list reddit user names to ignore posts from when
  selecting reddit submissions to cross post to Fediverse. This implements the feature
  request in issue #72

  To configure this add a section as below to your config:

.. codeblock:: ini
   [IgnoreUserList]
   bad_reddit_user:
   annoying-cross-poster:

Fixed
-----

- Issue #70: Creating hashtags from reddit usernames with a hyphen ("-") breaks the hashtag.
  Now replacing any hyphens with underscores.

.. _changelog-7.2.0:

7.2.0 — 2023-06-25
==================

Added
-----

- Config to optionally add a special hashtag for the reddit user making the post being re-shared on the Fediverse.
  If enabled this will add `posted by #u<reddit username>` at the end of the caption but just before any other hashtags
  This defaults to "false" / off if the setting doesn't exist; i.e. no change in behaviour for existing setups.
  To enable this new hashtag, add the following to the `config.ini` file in the `[Mastodon]` section:

  `UseRedditorTag: true`

Changed
-------

- Updated Dependencies

.. _changelog-7.1.1:

7.1.1 — 2023-05-21
==================

Changed
-------

- Optimized the publishing to PyPi step in CI.

Removed
-------

- Removed any specific code for collecting media from Gfycat. Gfycat seems to
  be no longer maintained and their SSL certificate has expired.

Fixed
-----

- Added some more error handling to fix issue #68

.. _changelog-7.1.0:

7.1.0 — 2023-05-20
==================

Added
-----

- Handling of rate limit errors of Imgur. Initially this consists of just logging rate limit errors and ignoring imgur
  images while Tootbot is rate limited by Imgur. Use the `--debug-log-file` command option to get most info.
  This partially addresses issue #65

Changed
-------

- Changed schema for history database to include a timestamp field. This timestamp field is used to find entries at
  least 31 days old that then are deleted at the end of the program execution. This should keep the history database
  from perpetually growing.

- Refactored how tootbot is dealing with media attachment files. It is now using python temporary files.

.. _changelog-7.0.3:

7.0.3 — 2023-03-05
==================

Changed
-------

- Updated dependencies

.. _changelog-7.0.2:

7.0.2 — 2023-02-19
==================

Changed
-------

- Updated dependencies which should also fix `issue 61`_.

.. _issue 61: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/61

- Dependency control now using `pdm`_ and releases build and published to Pypi with `flit`_

.. _pdm: https://pdm.fming.dev/latest/
.. _flit: https://flit.pypa.io/en/latest/

Removed
-------

- Removed poetry references and rstcheck, pip-audit and safety from pre-commit checking. Documentation, pip-audit and safety will still be checked as part of CI workflow.

.. _changelog-7.0.1:

7.0.1 — 2023-02-08
==================

Changed
-------

- Publish documentation to `Read the Docs`_

.. _Read the Docs: https://tootbot.readthedocs.io/en/latest/

- Some more debug logging

- Updated dependencies

.. _changelog-7.0.0:

7.0.0 — 2023-02-05
==================

Added
-----

- Added ability to make backlinks point to URL reddit post is linking to, instead of linking back to reddit post.
  This addresses `issue 57`_.

.. _issue 57: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/57

Changed
-------

- Now using Authorization Code / URL flow to generated access token.
  This is supported by `Takahe`_ (username and password flow is not)
  Addresses `issue 59`_

.. _Takahe: https://jointakahe.org
.. _issue 59: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/59

- Copied wiki content to docs folder with a view of retiring the Wiki eventually.

- Now using `ruff`_ for linting (replaces flake8 and some plugins)

.. _ruff: https://github.com/charliermarsh/ruff

- Updated dependencies

.. _changelog-6.2.2:

6.2.2 — 2023-01-21
==================

Added
-----

- Debug method to post a single reddit post. To use it call `tootbot_debug_submission` with command line
  arguments `--reddit-submission` and the reddit submission id (for example '10fg1xu' is a valid reddit submission id)
  This debug method also supports the `--config-dir` and `--debug-log-file` command line arguments the same way that
  the normal tootbot command does.
  Note: duplicate checks are disabled when using `tootbot_debug_submission`

Changed
-------

- Updated dependency versions.

Fixed
-----

- Refactored some error handling to fix `issue #56`_

.. _issue #56: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/56

- Replaced request.urlopen with aiohttp call to determine if a link points to an image or not. This change includes a
  timeout in case the server is blocking / refusing to return any content. This fixes `issue #58`_

.. _issue #58: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/58

.. _changelog-6.2.1:

6.2.1 — 2023-01-11
==================

Added
-----

- Added .editorconfig to set editor values

- Added `interrogate`_ to pre-commit checks and as a dev dependency to check all methods, classes, and modules have a docstring

.. _interrogate: https://interrogate.readthedocs.io/

- Added notification showing what mastodon account tootbot has authenticated to.
  This implements the feature request in `issue 55`_

.. _issue 55: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/55

Changed
-------

- Updated dependencies

- Now using `scriv`_ to maintain CHANGELOG

.. _scriv: https://scriv.readthedocs.io

Fixed
-----

- Address `issue 53`_ to gracefully handle when there is no gallery_data

.. _issue 53: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/53

6.2.0 - 2022-11-28
==================

Added
-----

- Allow for debug logging to file when using docker container by setting environment variable `DEBUG_LOG_FILE` to
  the file name for the log file
- Allow for no captions being added to toots. To disable captions set `UseCaption` to `false` in `Mastodon` section in
  `config.ini` file. This defaults to `true` if the setting isn't specified in the config file.
- Allow for no hashtags to be added to toots. To disable hashtags set `UseTags` to `false` in `Mastodon` section in
  `config.ini` file. This defaults to `true` if the setting isn't specified in the config file.
- Allow for no link back to reddit post to be added to toots. To disable hashtags set `UseBacklink` to `false` in
  `Mastodon` section in `config.ini` file. This defaults to `true` if the setting isn't specified in the config file.

Changed
-------

- Fixed issue #47 which sometimes caused toots to be posted without any media even when media_only was set to true.
- Fixed bare `#` being added to toots when a subreddit had no hashtags specified in `config.ini`
- Reddit and gfycat videos are now downloaded with an embedded copy of `yt-dlp`_.
  This fixes issue #48 and will now include audio on reddit videos.
- Added more debug logging
- Updated dependencies

.. _yt-dlp:  https://github.com/yt-dlp/yt-dlp

6.1.0 - 2022-10-22
==================

Changed
-------

- Tootbot will now respect the setting of `DelayBetweenPosts` between consecutive runs of tootbot.
- More detailed progress bars when downloading files and particularly when uploading files.
- Removed `rich`_ as a dependency as it and its dependencies are quite large.
- Replaced `Pillow`_ with `magic`_. Didn't need all functionality of pillow.
  Pillow and its dependencies are quite large.
- Removed need for Gfycat api secrets. No longer using gfycat python client api.

.. _rich: https://github.com/Textualize/rich
.. _Pillow: https://python-pillow.org/
.. _magic: https://github.com/ahupp/python-magic

6.0.2 - 2022-10-18
==================

Changed
-------

- Further improvements to docker image file size.
- Updated dependencies

6.0.1 - 2022-10-17
==================

Changed
-------

- Changed base image for Dockerfile. This makes the generated Docker image a lot smaller.

6.0.0 - 2022-10-16
==================

Changed
-------

- Now using my own `minimal-activitypub`_ package to talk to servers.
- Updated dependencies

.. _minimal-activitypub:  https://codeberg.org/MarvinsMastodonTools/minimal-activitypub

Added
-------

- Docker container definition. More info to follow.
- New setting `PostVisibility` in the `[Mastodon]` section for visibility of post made. Defaults to "public".
  Other possible values are "unlisted", and "private"

5.1.2 - 2022-08-21
==================

Changed
-------

- Fix for issue #46
- Replace all httpx uses with aiohttp. I have found httpx to be a little less reliable in some edge cases.
- Use aiofiles where reasonable
- use aiosqlite to make sqlite calls async as well.
- Updated dependencies

Added
-------

- `pip-audit`_ to CI pipeline

.. _pip-audit: https://pypi.org/project/pip-audit/

5.1.1 - 2022-08-14
==================

Changed
-------

- Fix for issue #44
- Updated dependencies

Added
-------

- Badges / Shields in README.md file
- Automatic publishing of new versions to PyPI.org

5.1.0 - 2022-07-31
==================

Changed
-------

- Refactored to use asyncio for getting posts of reddit (using asyncpraw instead of praw) and use asyncio for
  downloading images and videos.
- Progress bars are now rendered using `tqdm`_ as it supports async processing

.. _tqdm: https://github.com/tqdm/tqdm

Added
-------

- Perform version check on startup. Version is checked against the latest version published on PyPi.org

5.0.0 - 2022-05-01
==================

Breaking changes
----------------

- Keeping history of posts tooted in a SQLite database instead of a csv file. This history database is used internally
  to avoid posting duplicate content to Mastodon.
  See `Wiki`_ on how to migrate the content of your cache.csv file to the new SQLite database

.. _Wiki: https://codeberg.org/MarvinsMastodonTools/tootbot/wiki/Migrating-History-File

Changed
-------

- Refactored filtering of posts to be all in one method.

Added
-------

- Short script to convert existing csv file to new SQLite history database. This script can be invoked with the
  command: `tootbot_cache_2_sqlite`
- More static code checks by adding `Bandit`_, `Safety`_, and `flake8-annotations`_

.. _Bandit: https://pypi.org/project/bandit/
.. _Safety: https://pypi.org/project/safety/
.. _flake8-annotations: https://pypi.org/project/flake8-annotations/

Fixed
-------

- `Issue 40`_ by refactoring the startup sequence so that no progress bars are used until all API secrets are available.

.. _Issue 40: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/40

4.0.0 - 2022-04-05
==================

Fixed
-------

- Many incorrectly specified or missing type hints for `mypy`_

.. _mypy: http://mypy-lang.org/

Added
-------

- Using `Poetry`_ to maintain dependencies / requirements instead of requirements.txt file.
- Code formatting is now done using `black`_
- Implemented a number of pre-commit checks to improve / maintain code quality.
  See `pre-commit`_ and this projects `.pre-commit-config.yaml`_ file.
- Progress bars instead of the wall of info level log messages.
- Changelog (you are looking at it now :)

.. _Poetry: https://python-poetry.org/
.. _black:  https://black.readthedocs.io/en/stable/
.. _pre-commit: https://pre-commit.com/
.. _.pre-commit-config.yaml: https://codeberg.org/MarvinsMastodonTools/tootbot/src/branch/main/.pre-commit-config.yaml

Changed
-------

- Replaced all use of requests with `httpx`_

.. _httpx: https://www.python-httpx.org/

Removed
-------

- requirements.txt file as I now use Poetry instead.
- Version checking on start-up as tootbot is now available on PyPI and can easily be upgraded using pip or pipx.
- Deleting older toots. Please check out `MastodonAmnesia`_ as an alternative.
- Changed all info level log messages into debug level log messages

.. _MastodonAmnesia:  https://codeberg.org/MarvinsMastodonTools/mastodonamnesia
