# Dockerfile

FROM python:alpine

ARG WHEEL

LABEL maintainer="marvin8 <marvin8@tuta.io>"

COPY dist/$WHEEL /
COPY entry-point.sh /run/

# Install app and dependencies
WORKDIR /run
RUN apk add ffmpeg libmagic --virtual runtime-deps --no-cache \
    && apk add gcc g++ make libffi-dev openssl-dev --virtual build-deps --no-cache \
    && pip install /$WHEEL \
    && apk del build-deps --no-cache

ENTRYPOINT ["/run/entry-point.sh"]
