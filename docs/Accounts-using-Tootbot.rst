Accounts using Tootbot
======================

This is a list of accounts that are known to use Tootbot. If you have an account that uses Tootbot and would
like to have it listed here, please `create an issue`_ with the username, so I can add to this list.
(Only SFW accounts are permitted.)

* `@catsnkittens@botsin.space`_ - Show case account for most up to date version of tootbot from this repo.
  Have a look at the :doc:`Catsnkittens-Showcase-Config`.

* `@capybara@takahe.social`_ - /r/capybara

* `@babyelephantgifs@botsin.space`_ - /r/babyelephantgifs


.. _create an issue: https://codeberg.org/MarvinsMastodonTools/tootbot/issues
.. _@catsnkittens@botsin.space: https://botsin.space/@catsnkittens
.. _@capybara@takahe.social: https://takahe.social/@capybara@takahe.social/
.. _@babyelephantgifs@botsin.space: https://botsin.space/@babyelephantgifs
