Showcase Config for Catsnkittens
================================

Below is the current configuration for the Showcase Mastodon account:

.. code-block:: CFG
   :linenos:

   # This is the config file for Tootbot! You must restart the bot for any changes to take effect.

   # General settings
   [BotSettings]
   # File name for the cache spreadsheet (default is 'cache.csv')
   CacheFile: cache.csv
   # Minimum delay between social media posts, in seconds (default is '600')
   DelayBetweenPosts: 3593
   RunOnceOnly: false
   # Minimum position of post on subreddit front page that the bot will look at (default is '10')
   PostLimit: 5
   # Allow NSFW Reddit posts to be posted by the bot
   # NSFW media will be marked as sensitive, regardless of this setting
   NSFWPostsAllowed: false
   NSFWPostsMarked: false
   # Allow Reddit posts marked as spoilers to be posted by the bot
   SpoilersAllowed: true
   # Allow Reddit self-posts to be posted by the bot
   SelfPostsAllowed: false
   # List of hashtags to be used on every post, separated by commas without # symbols (example: hashtag1, hashtag2)
   # Leaving this blank will disable hashtags
   Hashtags: cat
   LogLevel: INFO

   [Subreddits]
   cats: cats
   kittens: kittens
   bodegacats: bodegacats


   # Settings related to promotional messages
   [PromoSettings]
   # How often should the promotional message be added
   # Setting is for a promotional message to be added every x messages
   # I.e. 0 = no promotional messages added ever
   #      1 = promotional message added to every new post
   #      2 = promotional message added to every 2nd new post
   #      n = promotional message added to every nth new post
   PromoEvery: 0
   # Actual Promotional message to be added
   PromoMessage: ...

   #Settings around Health Checks
   [HealthChecks]
   # This is the part of the URL before the unique id UID of the check. Could be something like
   # https://hc-ping.com or https://hc.example.com:8000/ping/
   # To disable Healthchecks leave the BaseUrl empty
   BaseUrl: <<redacted>>
   # This is the unique identified for the health check you set-up in your HealthChecks account.
   # It will be in the format: 5e9b16c5-27ce-4069-8317-05b78227c3a2
   UID: 5bdced0f-eca1-490e-bd6f-50e15bc7527d

   # Settings related to media attachments
   [MediaSettings]
   # Folder name for media downloads (default is 'media')
   MediaFolder: media
   # Set the bot to only post Reddit posts that directly link to media
   # Links from Gfycat, Giphy, Imgur, i.redd.it, and i.reddituploads.com are currently supported
   MediaPostsOnly: true

   # Mastodon settings
   [Mastodon]
   # Name of instance to log into (example: mastodon.social), leave blank to disable Mastodon posting
   InstanceDomain: botsin.space
   # Sets all media attachments as sensitive media, this should be left on 'true' in most cases (note: images from NSFW Reddit posts will always be marked as sensitive)
   # More info: https://gist.github.com/joyeusenoelle/74f6e6c0f349651349a0df9ae4582969#what-does-cw-mean
   SensitiveMedia: false
