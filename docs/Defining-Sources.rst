Defining Sources for Tootbot
=====================================

Tootbot can source posts to cross post from Subreddits or from reddit users (aka Redditors).

Tootbot checks two sections in the config file for what posts to cross post to the Fediverse. The first section is
named `[Subreddits]` and defines which subreddits to tootbot checks for new posts. The second second called `[Redditors]`
defines what reddit users tootbot checks for new posts. Both of these sections are formatted similarly.

Each section lists a source (subreddit name or user name) followed by a colon (`:`) and then optionally followed by
a comma separated list of hash tags (without quoting the `#`).

The following partial example config will cause tootbot to:

  - check `r/cats` and `r/kittens` subreddits for new "hot" posts
  - check for new posts in any subreddit by the reddit user "SunKisty275"
  - add the hash tag `#cats` to any posts originating from `r/cats`
  - add the hash tags `#cats` and `#kittens` to any posts originating from `r/kittens`
  - add the hash tag `#userpost` to any posts originating from the user `SunKisty275`

.. code-block:: ini

   [Subreddits]
   cats: cats
   kittens: cats, kittens

   [Redditors]
   SunKisty275: userpost
