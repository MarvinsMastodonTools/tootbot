Ignore Reddit Users
=====================================

Tootbot can skip any posts made by reddit users listed in the `[IgnoreUsersList]` section in the config file.

This is intended to be used for dealing with reddit users that regularly post content you don't want to cross post
(such as SPAM, rants, images not suitable for your audience, etc).

This section is lists reddit usernames followed by a colon (`:`) for whom you want to skip any posts.
This applies regardless of what sources you've defined. In particular if you have a reddit user listed in your
`[Redditors]` section and in your `[IgnoreUsersList]` section all posts from this user will be skipped / ignored.
I.e. tootbot will NOT cross post any of this users posts.

Please note, that the trailing colon after any username listed in the `[IgnoreUsersList]` section is important. Without
it, tootbot will fail.

The following partial example config will cause tootbot to:

  - skip / ignore any and all posts made by the reddit user with username "spamUser"
  - skip / ignore any and all posts made by the reddit user with username "rantyMcRantFace"

.. code-block:: ini

   [IgnoreUsersList]
   spamUser:
   rantyMcRantFace:
