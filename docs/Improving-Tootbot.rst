Improving / making changes to Tootbot
=====================================

I use `PDM`_ to keep track of and control dependencies / requirements for tootbot. If you want to make
changes to tootbot, I recommend you set up `PDM`_ on your system to make this easier.

After cloning the `Tootbot repository`_ use the command below to set up your virtual environment for Tootbot.
With this command, `PDM`_ will create a python virtual environment and install all dependencies / requirements
into it.

.. code-block:: console

   pdm sync --group :all



.. _PDM: https://pdm.fming.dev/latest/
.. _Tootbot repository: https://codeberg.org/MarvinsMastodonTools/tootbot
