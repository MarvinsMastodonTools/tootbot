Migrating History File
======================

In version 5.0.0 Tootbot changed from using a CSV file to keep track of history to a SQLite database to do so.
This history record is being used to prevent sharing duplicates of the same post, link, or media files.

Stating with version 5.0.0 Tootbot includes a simple tool to convert the history recorded in the CSV file into
corresponding entries in the SQL database used from version 5.0.0 onwards.

To run the tool simple use the command

.. code-block:: console

   tootbot_cache_2_sqlite
