Installing Tootbot
==================

As Tootbot is now on `PyPI`_ the easiest way to install Tootbot is by using Python's pip tool like below:

.. code-block:: console

   pip install tootbot


First run of Tootbot
--------------------

Before running Tootbot, generate a template `config.ini` file by using the following command:

.. code-block:: console

   tootbot_create_config

This will create a sample `config.ini` file in the current directory that you should then edit to suit your needs.

Once installed and configured you can start Tootbot with the command:

.. code-block:: console

   tootbot

On the first run, Tootbot will ask you for details as below:
1) Mastodon account Tootbot should post to
2) Reddit account details to use to access subreddits
3) Imgur account to use to download linked media from Imgur

Tootbot will store the relevant account details in a number of files ending in `.secret` in the current directory and
will not ask for account details again.

Once Tootbot has collect all needed account details it will then start searching the subreddits defined in the
`config.ini` file and post a toot to the configured Mastodon account.

.. _PyPI: https://pypi.org/project/tootbot/
