Downloading Tootbot and installing dependencies / requirements
==============================================================

The easiest way to use Tootbot is by installing from `PyPi`_ using `pipx`_ or `pip`_.

I highly recommend using `pipx`_ if your system supports it. Installing Tootbot with `pipx`_ is as simple as
typing the following into a command line / terminal window:

.. code-block:: console

   pipx install tootbot

This command will download and install tootbot and all its dependencies / requirements from `PyPi`_

Settings
--------

All settings are expected to be defined in the `config.ini` file in the current directory. If you are starting from
scratch you can create a sample `config.ini` file using the command line:

.. code-block:: console

   tootbot_create_config

This will create a `config.ini` file in the current directory that you now need to edit to add your settings.

Settings in the config file
---------------------------

All settings for the bot can be found in the `config.ini` file. As an example you can look at the
:doc:`Catsnkittens-Showcase-Config` used for the showcase account `@catsnkittens@botsin.space`_.
Open the file in any text editor and perform the following tasks:

1. Define your sources. See :doc:`Defining-Sources` for details on how to do that.

2. By default, the bot will wait at least 600 seconds (10 minutes) between posts to prevent spamming. You can change
   this by editing the `DelayBetweenTweets` setting in the `[BotSettings]` section.

3. By default, the bot will only look at the top 10 'hot' posts in a subreddit. You can change this by editing the
   `PostLimit` setting in the `[BotSettings]` section.

4. You can enable or disable NSFW posts and self-posts, by changing `NSFWPostsAllowed` and `SelfPostsAllowed` to
   `true` or `false`.

5. If you want Tootbot to only post media (images, GIFs, GIFV files, etc), set `MediaPostsOnly` to `true`.

6. Add the name of your instance (e.g. mastodon.social) to `InstanceDomain` in the `[Mastodon]` section.

Entering API information
------------------------

Next, you need to run Tootbot to setup API access with Reddit, Imgur, and Mastodon. Below is an explanation for what
each service is used for:

* The Reddit API is used to grab posts from subreddits.
* The Imgur API is used to grab direct image URLs from gallery/album links.
* Mastodon API access is required for posting to social media.

Open the command line/terminal and set your directory to the Tootbot folder. Then type this command:

.. code-block:: console

   tootbot

After Tootbot starts up, it will begin asking for API keys. You only have to do this process once.

Setting up Reddit API access
++++++++++++++++++++++++++++

1. Log into Reddit, go to your `app preferences`_, and click the 'Create a new application' button at the bottom.
2. Select 'script' as the application type, and click the 'Create app' button.
3. You should see a Reddit agent string (underneath 'personal use script') and an agent secret. Paste these into
   Tootbot when it asks for them, pressing Enter after each one.


|Reddit API keys|

If you ever want to re-setup Reddit access (if you make a new account, for example), just delete the `reddit.secret`
file and run Tootbot again.

Setting up Imgur API access
+++++++++++++++++++++++++++

1. Sign into `Imgur`_ with your account, or make one if you haven't already.
2. Register an application  by `adding a client`_ and choose 'OAuth 2 authorization without a callback URL' as the app type.
3. Imgur will give you a Client ID and Client Secret. Paste these into Tootbot when it asks for them, pressing Enter
   after each one.

If you ever want to re-setup Imgur access (if you make a new account, for example), just delete the `imgur.secret` file
and run Tootbot again.

Setting up Mastodon API access
++++++++++++++++++++++++++++++

Go to the special authorization URL of your Fediverse instance displayed by tootbot. Follow the instructions on that
site to generate an Authorization Code. Finally, supply that Authorization Code to tootbot.

If you ever want to re-setup Mastodon access (if you make a new account, for example), just delete the
`mastodon.secret` file and run Tootbot again.

Running Tootbot
---------------

Now that you have everything set up, Tootbot should start posting to Mastodon. For more details read
:doc:`Running-Tootbot`.

Once you're done making a bot account with Tootbot, please add it to the list of :doc:`Accounts-using-Tootbot`!


.. _PyPi: https://pypi.org/
.. _pipx: https://pypa.github.io/pipx/
.. _pip: https://pip.pypa.io/en/stable/
.. _sample config: Catsnkittens-Showcase-Config.md
.. _@catsnkittens@botsin.space: https://botsin.space/@catsnkittens
.. _app preferences: https://www.reddit.com/prefs/apps
.. _Imgur: https://imgur.com/
.. _adding a client: https://api.imgur.com/oauth2/addclient

.. |Reddit API keys| image:: z6HSpVQ.png
    :alt: Image of Reddit Api keys
