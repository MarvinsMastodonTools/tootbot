Throttling
==========

When and why throttle
---------------------

The mastodon server you've connected tootbot to might encounter some errors. One that I've seen a few times, are errors
to do with posting attachments when the servers storage back end encounters problems. Often that is reported as a
server error 500 by the mastodon API. Sometimes it can take the maintainer of the mastodon server some time to rectify
such a situation.

My thinking is that during an extended issue with a mastodon server it does not make sense to keep on trying to post
toots. I believe it is reasonable to slow down posting while the Mastodon API reports errors.

How does throttling in tootbot work
-----------------------------------

Tootbot will only throttle on errors reported by the mastodon API.

Tootbot will add an extra delay between posts of the length of `DelayBetweenPosts` specified in your `config.ini` file
for every consecutive error reported by the mastodon API.

There is an additional parameter in the `config.ini` file called `ThrottlingMaxDelay` (in seconds) which is the maximum
delay that tootbot will use for throttling.


Example:
........

If you've specified 300 (seconds) for `DelayBetweenPosts` (i.e. 5 minutes), tootbot will wait an extra
5 minutes, in addition to the standard delay of 5 minutes. So after the first error reported by the Mastodon API,
tootbot will wait 10 minutes in total before attempting to post again.

If the mastodon API reports another error after attempting to post after the first error, tootbot will add another
additional delay of (in this example) 5 minutes, making the total delay after the second error 15 minutes.

Tootbot calculates any additional delay as follows:
Total additional delay (in seconds) = `DelayBetweenPosts` + (number_of_consecutive_errors * `DelayBetweenPosts`)

As soon as tootbot has been able to successfully post to the configured mastodon server, without the API reporting an
error, tootbot will no longer add any additional delays and counting of consecutive errors starts at 0 again.

If there is an extended problem with posting to the configured mastodon server, tootbot will not wait between toot
attempts any longer then specified with the `ThrottlingMaxDelay` in the config file. In my tootbot instances I have
this set to 86400 (seconds), which equates to 24 hours or 1 day.
