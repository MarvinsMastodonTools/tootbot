.. Tootbot documentation master file, created by
   sphinx-quickstart on Wed Feb  8 10:16:07 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tootbot's documentation!
===================================

Setting up Tootbot
------------------

For instructions on how to set-up Tootbot for the first time read :doc:`Setting-up-Tootbot`

Specifics
---------

There is more information on :doc:`Running-Tootbot`, :doc:`Defining-Sources`, :doc:`Ignore-Reddit-User`,
:doc:`Throttling`, :doc:`Migrating-History-File` and on :doc:`Accounts-using-Tootbot`

Other info
----------

For information on how to help improve tootbot read :doc:`Improving-Tootbot`

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2

   Setting-up-Tootbot
   Running-Tootbot
   Defining-Sources
   Ignore-Reddit-User
   Throttling
   Migrating-History-File
   Accounts-using-Tootbot
   Catsnkittens-Showcase-Config
   Improving-Tootbot

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
