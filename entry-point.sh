#!/bin/sh
set -e

cd /run

if test -z "${DEBUG_LOG_FILE}"; then
  tootbot --config-dir /config
else
  tootbot --config-dir /config --debug-log-file /config/${DEBUG_LOG_FILE}
fi
