# ruff: noqa: D100, D103
import pathlib
import shutil

import nox

nox.options.reuse_existing_virtualenvs = True
nox.options.sessions = [
    "mypy",
    "ruff",
    "interrogate",
    "pip_audit_safety",
    "refactor",
]


@nox.session(python=["3.8", "3.9", "3.10", "3.11", "3.12"])
def mypy(session):
    session.install("mypy", "types-toml", "types-aiofiles")
    session.run("mypy", "src/")


@nox.session
def ruff(session):
    session.install("ruff")
    session.run("ruff", "format", "--check", "src/")
    session.run("ruff", "src/")


@nox.session
def refactor(session):
    session.install("codelimit")
    session.run("codelimit", "check", "src")


@nox.session
def interrogate(session):
    session.install("interrogate")
    session.run("interrogate", "-vv", "src/")

    pathlib.Path("interrogate_badge.svg").unlink()


@nox.session
def pip_audit_safety(session):
    session.install("pdm", "safety", "pip-audit")
    session.run(
        "pdm",
        "export",
        "--prod",
        "--format",
        "requirements",
        "--output",
        "requirements.txt",
    )
    session.run("safety", "check", "-r", "requirements.txt")
    session.run("pip-audit", "-r", "requirements.txt")

    pathlib.Path("requirements.txt").unlink()


@nox.session
def build_docs(session):
    session.install("Sphinx", "sphinx-rtd-theme")
    session.run(
        "git",
        "branch",
        "--show-current",
        external=True,
    )
    session.run(
        "sphinx-build",
        "-M",
        "clean",
        "docs",
        "docs/_build",
    )
    session.run(
        "sphinx-build",
        "-M",
        "html",
        "docs",
        "docs/_build",
    )
    session.run(
        "git",
        "stash",
        external=True,
    )
    session.run(
        "git",
        "checkout",
        "pages",
        external=True,
    )
    shutil.copytree(
        src="docs/_build/html",
        dst=".",
        dirs_exist_ok=True,
    )
    session.run(
        "git",
        "status",
        "--porcelain",
        "-uno",
        external=True,
    )
